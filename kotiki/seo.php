<?php
global $wpdb;

$image = $wpdb->get_results("SELECT guid FROM {$wpdb->posts} WHERE post_parent = {$post->ID} and post_type='attachment'");
$image = get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : 'https://makeit.technology/wp-content/uploads/2020/04/header-logo.svg';

if(is_home() && is_front_page()): ?>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Organization",
            "name": "makeit.technology",
            "url": "https://makeit.technology/",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "Ivana Rubchaka street, 23",
                "addressRegion": "Lviv",
                "addressCountry": "UA"
            },
            "logo": {
                "@type": "ImageObject",
                "contentUrl": "https://makeit.technology/wp-content/uploads/2020/04/header-logo.svg"
            },
            "contactPoint": {
                "@type": "contactPoint",
                "contactType": "Customer Service",
                "telephone": "+38-067-672-99-69",
                "email": "e@makeit.technology"
            }
        }
    </script>
<?php elseif ( is_single()) : ?>
    <script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "Article",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "<?php echo get_page_link();?>"
		},
		"headline": "<?php echo the_title(); ?>",
		"description": "<?php echo get_post_meta($post->ID, "
		_yoast_wpseo_metadesc ", true); ?>",
		"image": "<?php echo $image;?>",
		"author": {
			"@type": "Person",
			"name": "MakeIT"
		},
		"publisher": {
			"@type": "Organization",
			"name": "MakeIT",
			"logo": {
				"@type": "ImageObject",
				"url": "https://makeit.technology/wp-content/uploads/2020/04/header-logo.svg"
			}
		},
"datePublished": "<?php the_time('Y-m-d\'T\'H:m:s'); ?>",
"dateModified": "<?php the_time('Y-m-d\'T\'H:m:s'); ?>"

	}
	</script>
    <script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement": [{
			"@type": "ListItem",
			"position": 1,
			"name": "Main",
			"item": "<?php echo get_site_url(); ?>"
		}, {
			"@type": "ListItem",
			"position": 2,
			"name": "<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>",
			"item": "<?php $cat_url = get_the_category(); echo get_term_link($cat_url[0]); ?>"
		}, {
			"@type": "ListItem",
			"position": 3,
			"name": "<?php echo the_title(); ?>",
			"item": "<?php echo get_page_link();?>"
		}]
	}
	</script>
<?php else: ?>
    <script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement": [{
			"@type": "ListItem",
			"position": 1,
			"name": "Main",
			"item": "<?php echo get_site_url(); ?>"
		}, {
			"@type": "ListItem",
			"position": 2,
			"name": "<?php echo the_title(); ?>",
			"item": "<?php echo get_page_link();?>"
		}]
	}
	</script>
<?php endif; ?>

<?php  if(is_page()) { ?>
<script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "Article",
		"mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "<?php echo get_page_link();?>"
		},
		"headline": "<?php echo the_title(); ?>",
		"image": "<?php echo $image;?>",
		"author": {
			"@type": "Organization",
			"name": "MakeIT"
		},
		"publisher": {
			"@type": "Organization",
			"name": "MakeIT",
			"logo": {
				"@type": "ImageObject",
				"url": "https://makeit.technology/wp-content/uploads/2020/04/header-logo.svg"
			}
		},
        "datePublished": "<?php the_time('Y-m-d\'T\'H:m:s'); ?>",
        "dateModified": "<?php the_time('Y-m-d\'T\'H:m:s'); ?>"

	}
	</script>
<?php } ?>
