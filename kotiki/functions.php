<?php
/**
 * kotiki functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kotiki
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

// создаем новую колонку
add_filter( 'manage_'.'post'.'_posts_columns', 'add_views_column', 4 );
function add_views_column( $columns ){
    $num = 5; // после какой по счету колонки вставлять новые

    $new_columns = array(
        'wordcount' => 'WordCounts',
    );

    return array_slice( $columns, 0, $num ) + $new_columns + array_slice( $columns, $num );
}

// заполняем колонку данными
// wp-admin/includes/class-wp-posts-list-table.php
add_action('manage_'.'post'.'_posts_custom_column', 'fill_views_column', 5, 2 );
function fill_views_column( $colname, $post_id ){
    $content = get_post_field( 'post_content', $post_id ); // Get the content
    $wordcount = str_word_count( strip_tags( $content ) ); // Count the words
    if( $colname === 'wordcount' ){
        echo $wordcount;
    }
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kotiki_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on kotiki, use a find and replace
		* to change 'kotiki' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'kotiki', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'kotiki' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Set up the WordPress core custom background feature.
	add_theme_support(
		'custom-background',
		apply_filters(
			'kotiki_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'kotiki_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kotiki_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kotiki_content_width', 640 );
}
add_action( 'after_setup_theme', 'kotiki_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kotiki_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'kotiki' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'kotiki' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'kotiki_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function kotiki_scripts() {
	wp_enqueue_style( 'kotiki-style', get_stylesheet_uri(), array(), _S_VERSION );

        wp_register_script(
            'sorting_posts',get_stylesheet_directory_uri() . '/js/sorted.js'
        );

        wp_localize_script('sorting_posts',
            'sorting',
            array( 'sorting_url' => admin_url( 'admin-ajax.php' ) )
        );

        wp_enqueue_script( 'sorting_posts' );


}
add_action( 'wp_footer', 'kotiki_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function posts() {
    $paged = ! empty( $_POST[ 'paged' ] ) ? $_POST[ 'paged' ] : 0;
    $paged++;

    $args = array(
        'post_type' => 'post',
        'paged' => $paged,
        'post_status' => 'publish'
    );

    if (isset($_POST['orderby'])) {
        $args['orderby'] = $_POST['orderby'];
        if ($_POST['orderby'] === 'meta_value_num') {
            $args['meta_key'] = 'wordcount';
        }
    }
    if (isset($_POST['sorted'])) {
        $args['order'] = $_POST['sorted'];
    }

    query_posts( $args );

    while( have_posts() ) : the_post();

        get_template_part( 'template-parts/content');

    endwhile;
}

add_action( 'wp_ajax_sorted_loadmore', 'sorted_loadmore' );
add_action( 'wp_ajax_nopriv_sorted_loadmore', 'sorted_loadmore' );

function sorted_loadmore() {
    posts();
    die;
}

add_action( 'wp_ajax_sorted_posts', 'sorting_posts' );
add_action( 'wp_ajax_nopriv_sorted_posts', 'sorting_posts' );

function sorting_posts() {
    global $wp_query;

    posts();

    echo '<div id="loadmore" style="text-align:center;">
            <button data-max_pages="' . $wp_query->max_num_pages . '" data-paged="1" class="button loadmore-link">Загрузить ещё</button>
        </div>';

    die;
}

add_action('admin_menu', 'top_tags_register_admin_page');
add_action('admin_init', function () {
    register_setting('sort_options_group', 'sorting_option');
});

function top_tags_register_admin_page() {
    add_submenu_page(
        'edit.php',
        'Сортировка постов',
        'Сортировка постов',
        'manage_categories',
        'top-tags',
        'top_tags_render_admin_page'
    );
}

function top_tags_render_admin_page() {

    include 'admin-page.php';

}

add_action( 'save_post', 'add_post_wordcount' );
add_action( 'post_updated', 'add_post_wordcount' );

function add_post_wordcount( $post_id ) {
    $content = get_post_field( 'post_content', $post_id ); // Get the content
    $wordcount = str_word_count( strip_tags( $content ) ); // Count the words
    if ( ! add_post_meta( $post_id, 'wordcount', $wordcount, true ) ) {
        update_post_meta( $post_id, 'wordcount', $wordcount );
    }
}

