let select = document.getElementById('select_options');
let post = document.querySelector('.primary-page__content');
let option = null;
let orderby = null;

function bindLoadMoreEvent () {
    let ajaxload = document.querySelector('.loadmore-link'),
        paged = ajaxload.dataset.paged,
        maxPages = ajaxload.dataset.max_pages;

    ajaxload.addEventListener('click', function () {
        let formData = new FormData();
        formData.set('action', 'sorted_loadmore');
        formData.set('paged', paged);
        if (orderby !== null) {
            formData.set('orderby', orderby);
        }
        if (option !== null) {
            formData.append('sorted', option);
        }

        fetch(sorting.sorting_url, {
            method: 'POST',
            body: formData
        })
        .then(r => {
            return r.text()
        })
        .then((data) =>  {
            let loadmore = document.querySelector('#loadmore');
            loadmore.insertAdjacentHTML('beforebegin', data);
            paged++;

            if( paged == maxPages ) {
                ajaxload.remove();
            }
        })
        .catch((error) => {
            console.log(error)
        });
    });
}

bindLoadMoreEvent();

select.addEventListener('change', function () {
    let formData = new FormData();
    option = select.options[select.selectedIndex].value;
    orderby = select.dataset.orderby;
    formData.set('orderby', orderby);
    formData.set('action', 'sorted_posts');
    formData.set('sorted', option);

    fetch(sorting.sorting_url, {
        method: 'POST',
        body: formData
    })
    .then(r => {
        return r.text()
    })
    .then((data) =>  {
        post.innerHTML = data;
        bindLoadMoreEvent();
    })
    .catch((error) => {
        console.log(error)
    });
});


