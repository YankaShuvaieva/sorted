<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kotiki
 */

get_header();
global $wp_query;

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;

if (get_option('sorting_option') === 'on') { ?>
    <select id="select_options" data-orderby="meta_value_num">
        <option value="" selected disabled>Выберите метод сортировки</option>
        <option value="DESC">Сортировать от большего к меньшему</option>
        <option value="ASC">Сортировать от меньшего к большему</option>
    </select>
<?php } else { ?>
    <select id="select_options" data-orderby="title">
        <option value="" selected disabled>Сортировка по названию поста</option>
        <option value="ASC">Сортировать от А до Я</option>
        <option value="DESC">Сортировать от Я до А</option>
    </select>
<?php } ?>



	<main id="primary" class="site-main">

        <div class="primary-page__content">

		<?php

		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
                the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;


            // если текущая страница меньше, чем максимум страниц, то выводим кнопку
            if( $paged < $max_pages ) :
                echo '<div id="loadmore" style="text-align:center;">
            <button data-max_pages="' . $max_pages . '" data-paged="' . $paged . '" class="button loadmore-link">Загрузить ещё</button>
        </div>';
            endif;


		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

        </div>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
