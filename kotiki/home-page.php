<?php
/**
    Template Name: Home template
 */

get_header();
global $wp_query;

$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$max_pages = $wp_query->max_num_pages;

$query = new WP_Query(
    array(
        'post_type' => 'post',
        'orderby' => 'title', // сортировка по дате у нас будет в любом случае (но вы можете изменить/доработать это)
        'order'	=> $_POST['sort'] // ASC или DESC
    )
);

?>

    <main id="primary" class="site-main">

        <div class="primary-page__content">

            <?php

            if ( $query->have_posts() ) :

//                if ( is_home() && ! is_front_page() ) :
//                    ?>
<!--                    <header>-->
<!--                        <h1 class="page-title screen-reader-text">--><?php //single_post_title(); ?><!--</h1>-->
<!--                    </header>-->
                <?php
//                endif;

                /* Start the Loop */
                while ( $query->have_posts() ) :
                    $query->the_post();

                    /*
                     * Include the Post-Type-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                     */
                    get_template_part( 'template-parts/content', get_post_type() );

                endwhile;


                // если текущая страница меньше, чем максимум страниц, то выводим кнопку
                if( $paged < $max_pages ) :
                    echo '<div id="loadmore" style="text-align:center;">
            <a href="#" data-max_pages="' . $max_pages . '" data-paged="' . $paged . '" class="button loadmore-link">Загрузить ещё</a>
        </div>';
                endif;


            else :

                get_template_part( 'template-parts/content', 'none' );

            endif;
            ?>

        </div>

    </main><!-- #main -->

<?php
get_sidebar();
get_footer();
